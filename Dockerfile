FROM frolvlad/alpine-oraclejdk8
ENV prueba=/test
RUN mkdir ${prueba}
COPY Main.jar ${prueba}/app.jar
WORKDIR ${prueba}
CMD ["java", "-jar", "app.jar"]


#FROM openjdk:7
#COPY . /usr/docker/Calcular
#WORKDIR /usr/docker/Calcular
#RUN javac Main.java
#CMD ["java", "Main"]